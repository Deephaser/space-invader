package main;

public class SpaceInvaders {
    private int longueur;
    private int hauteur;
    public Vaisseau vaisseau;
    public SpaceInvaders(int longueur, int hauteur)
    {
        this.longueur = longueur;
        this.hauteur =hauteur;
    }

    public String recupererEspaceJeuDansChaineASCII()
    {
        StringBuilder espaceDeJeu = new StringBuilder();
        for (int y = 0; y< hauteur; y++)
        {
            for (int x = 0; x< longueur; x++)
            {
                char marque;
                marque = recupererMarqueDeLaPosition(x, y);
                espaceDeJeu.append(marque);
            }
            espaceDeJeu.append("\n");
        }
        return espaceDeJeu.toString();
    }

    private char recupererMarqueDeLaPosition(int x, int y) {
        char marque;
        if (this.aUnVaisseauQuiOccupeLaPosition(x, y))
            marque='V';
        else
            marque='.';
        return marque;
    }

    public boolean aUnVaisseauQuiOccupeLaPosition(int x, int y)
    {
        return vaisseau!=null && this.vaisseau.occupeLaPosition(x, y);
    }

    public void positionnerUnNouveauVaisseau(int longueur, int hauteur, int x, int y) {
        if (!estDansEspaceJeu(x, y))
            throw new HorsEspaceJeuException("La position du vaisseau est en dehors de l'espace jeu");

        if ( !estDansEspaceJeu(x+longueur-1,y))
            throw new DebordementEspaceJeuException("Le vaisseau déborde de l'espace jeu vers la droite à cause de sa longueur");
        if (!estDansEspaceJeu(x,y-hauteur+1))
            throw new DebordementEspaceJeuException("Le vaisseau déborde de l'espace jeu vers le bas à cause de sa hauteur");

        vaisseau = new Vaisseau(longueur, hauteur);
        vaisseau.positionner(x, y);
    }

    public boolean estDansEspaceJeu(int x,int y)
    {
        return(!((x >= longueur || x <0) || (y >= hauteur || y<0)));
    }

    public void deplacerVaisseauVersLaDroite() {
        if (vaisseau.abscisseLaPlusADroite() < (longueur - 1))
            vaisseau.seDeplacerVersLaDroite();
    }
    public void deplacerVaisseauVersLaGauche()
    {
        if (vaisseau.abscisseLaPlusAGauche() > 0) vaisseau.seDeplacerVersLaGauche();
    }
}
