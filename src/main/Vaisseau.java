package main;

public class Vaisseau {
    private int x;
    private int y;
    private int longueur;
    private int hauteur;

    public Vaisseau(int longueur, int hauteur, int x, int y) {
        setLongueur(longueur);
        setHauteur(hauteur);
        setX(x);
        setY(y);
    }
    public Vaisseau(int longueur, int hauteur) {
        this(longueur, hauteur, 0, 0);
    }

    public void setLongueur(int longueur) {
        this.longueur = longueur;
    }

    public void setHauteur(int hauteur) {
        this.hauteur = hauteur;
    }

    public int getLongueur() {
        return longueur;
    }

    public int getHauteur() {
        return hauteur;
    }

    public int abscisseLaPlusAGauche() {
        return x;
    }

    public int ordonneeLaPlusBasse() {
        return y;
    }

    public void setX(int x) { this.x = x; }

    public void setY(int y) { this.y = y; }

    public boolean occupeLaPosition(int x, int y) {
        return (estAbscisseCouverte(x) && estOrdonneeCouverte(y));
    }

    public boolean estAbscisseCouverte(int x)
    {
        return ((abscisseLaPlusAGauche()<=x) && (x<=abscisseLaPlusADroite()));
    }
    public boolean estOrdonneeCouverte(int y)
    {
        return ((( ordonneeLaPlusHaute()<= y) && (y <= ordonneeLaPlusBasse())));
    }

    public void positionner(int x, int y) {
        setX(x);
        setY(y);
    }

    public void seDeplacerVersLaDroite() {
        setX(abscisseLaPlusAGauche()+1);
    }

    public void seDeplacerVersLaGauche() {
        setX(abscisseLaPlusAGauche()-1);
    }

    public int abscisseLaPlusADroite() {
        return abscisseLaPlusAGauche()+getLongueur()-1;
    }

    public int ordonneeLaPlusHaute() {
        return (ordonneeLaPlusBasse() - getHauteur() + 1);
    }
}
